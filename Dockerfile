# DOCKER-VERSION 0.6.4

FROM	centos:6.4
MAINTAINER Ryan Dick "noxinoobin@gmail.com"

# Enable EPEL
RUN	rpm -Uvh http://download.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm

# Install dev tools
RUN	yum groupinstall -y "Development Tools" && yum install -y kernel-devel kernel-headers make git 

# Download and install nodejs
RUN	cd /tmp && wget http://nodejs.org/dist/v0.10.21/node-v0.10.21-linux-x64.tar.gz && cd /usr/local && tar --strip-components 1 -xzf /tmp/node-v0.10.21-linux-x64.tar.gz

# Check Node Install
RUN	node -v && npm -v

# Install Node Build Tools
RUN	npm i -g bower grunt-cli npm

# Add Assets
ADD	./assets /tmp/assets
ADD	./app /app
ADD	./keys /tmp/keys

# Add MongoDB-10gen repo, install mongo and start service
#RUN	cd /tmp && wget -i /tmp/assets/mongo-rpm-links&& rpm -iUv /tmp/*.rpm&& rm /tmp/*.rpm&& service mongod start&& chkconfig mongod on 

# Install keys
RUN	mkdir -p /root/.ssh
RUN cp /tmp/keys/* /root/.ssh/

# Expose ports
EXPOSE  5000

# Install deps
RUN	cd /app && /usr/local/bin/npm i -d

# Run App
CMD ["/usr/local/bin/node", "/app/web.js"]

