#!/bin/bash

echo "getting container id..."
container_id=`docker run -d ryandick/node-dock ls`
echo "container id is: $container_id, exporting and compressing..."
docker export $container_id | gzip -9 > node-dock-image.tar.gz
echo "done image size: "
du -h node-dock-image.tar.gz
echo "uploading to dropbox..."
./dropbox_uploader.sh upload node-dock-image.tar.gz /node-dock-image.tar.gz -d
